# coding: utf-8

import socket
 
UDP_IP="127.0.0.1"
UDP_PORT=7070
 
sock = socket.socket( socket.AF_INET, # Internet
                      socket.SOCK_DGRAM ) # UDP
sock.bind( (UDP_IP,UDP_PORT) )

# словать который будет хранить список клиентов.
client_addresses_list = {}
 
while True:
    client_text, client_address = sock.recvfrom(1024)  # buffer size is 1024 bytes
    client_ip, client_port = client_address

    client_key = "client-ip(" + str(client_ip) + ")-port(" + str(client_port) + ")"
    print "\n>>>нам прислал сообщение клиент:  \t", client_key
    print ">>> текст сообщения:               \t", client_text

    client_addresses_list[client_key] = client_address
    print ">>> количетсво всех клиентов:      \t", len(client_addresses_list)
    print ">>> список всех клиентов:          \t", client_addresses_list

    # разсылаем полученное сообщение от одного клиента всем остальным:
    for key, address in client_addresses_list.items():
        if key != client_key:
            print "отправляем клиенту:", key, 'сообщение: [', client_text, ']'
            sock.sendto(client_text, address)



